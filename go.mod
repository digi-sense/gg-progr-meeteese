module bitbucket.org/digi-sense/gg-progr-meeteese

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.40
	bitbucket.org/digi-sense/gg-core-x v0.1.40
	bitbucket.org/digi-sense/gg-module-mailboxer v0.1.9
	github.com/cbroglie/mustache v1.3.1
	github.com/gofiber/fiber/v2 v2.25.0 // indirect
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	gorm.io/driver/sqlite v1.2.6
	gorm.io/gorm v1.22.5
)
