package meeteese_commons

import "strings"

// ---------------------------------------------------------------------------------------------------------------------
//	r e f e r r a l s
// ---------------------------------------------------------------------------------------------------------------------

const (
	RoleApiAll        = "api-all"         // all api enabled
	RoleApiEmail      = "api-email"       // enable email commands
	RoleApiWeb        = "api-web"         // enable web api
	RoleApiCreateUser = "api-create-user" // enable user creation
)

// ---------------------------------------------------------------------------------------------------------------------
//	c o m m a n d s
// ---------------------------------------------------------------------------------------------------------------------

const (
	CmdGetSlotsFree = "slots_free"
	CmdGetSlotsOne  = "slots_one"
	CmdSlotsUpdate  = "slots_update"

	CmdUsersCreate = "users_create"
)

var (
	AllCommands = []string{
		CmdGetSlotsFree, CmdGetSlotsOne, CmdSlotsUpdate,
		CmdUsersCreate,
	}
)

// ---------------------------------------------------------------------------------------------------------------------
//	a p i
// ---------------------------------------------------------------------------------------------------------------------

var (
	GET_SLOTS_FREE = strings.ReplaceAll("/api/v1/"+CmdGetSlotsFree, "_", "/")
	GET_SLOTS_ONE  = strings.ReplaceAll("/api/v1/"+CmdGetSlotsOne, "_", "/") // "/api/v1/slots/one"
	SLOTS_UPDATE   = strings.ReplaceAll("/api/v1/"+CmdSlotsUpdate, "_", "/")

	USERS_CREATE = strings.ReplaceAll("/api/v1/"+CmdUsersCreate, "_", "/")
)
