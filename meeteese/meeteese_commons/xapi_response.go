package meeteese_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"errors"
	"fmt"
)

type MeeteeseApiResponse struct {
	Referral  string                        `json:"referral"`   // channel who called the api. values: "https", "email"
	Sender    string                        `json:"sender"`     // user who called the api. Email or user ID
	UserId    uint                          `json:"user_id"`    // identified user
	UserEmail string                        `json:"user_email"` // identified user
	Command   *MeeteeseApiStatement         `json:"command"`    // original command sent
	Response  *MeeteeseApiStatementResponse `json:"response"`   // command response
}

func (instance *MeeteeseApiResponse) String() string {
	return gg.JSON.Stringify(instance)
}

type MeeteeseApiStatement struct {
	Statement string                 `json:"statement"`
	Params    map[string]interface{} `json:"params"`
}

type MeeteeseApiStatementResponse struct {
	Error        string      `json:"error"`
	ErrorMessage string      `json:"error_message"`
	Result       interface{} `json:"result"`
}

func (instance *MeeteeseApiStatementResponse) IsError() bool {
	return len(instance.Error) > 0 || len(instance.ErrorMessage) > 0
}

func (instance *MeeteeseApiStatementResponse) GetError() error {
	if nil != instance && instance.IsError() {
		if len(instance.Error) > 0 {
			return errors.New(fmt.Sprintf("%s:%s", instance.Error, instance.ErrorMessage))
		}
		return errors.New(fmt.Sprintf("%s", instance.ErrorMessage))
	}
	return nil
}
