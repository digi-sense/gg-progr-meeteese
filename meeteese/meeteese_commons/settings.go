package meeteese_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"bitbucket.org/digi-sense/gg-core/gg_email"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
	"fmt"
)

type MeeteeseSettings struct {
	Postman  *MeeteesePostmanSettings  `json:"postman"`
	Schedule *MeeteeseScheduleSettings `json:"schedule"`
}

type MeeteesePostmanSettings struct {
	SMS   *MeeteeseSettingsSMS   `json:"sms"`
	Email *MeeteeseSettingsEmail `json:"email"`
}

type MeeteeseScheduleSettings struct {
	gg_scheduler.Schedule
}

type MeeteeseSettingsSMS struct {
	gg_sms_engine.SMSConfiguration
}

func (instance *MeeteeseSettingsSMS) String() string {
	return gg.JSON.Stringify(instance)
}

type MeeteeseSettingsEmail struct {
	Send *MeeteeseSettingsSend `json:"send"`
	Read *MeeteeseSettingsRead `json:"read"`
}

type MeeteeseSettingsSend struct {
	gg_email.SmtpSettings
	Enabled bool `json:"enabled"`
}

func (instance *MeeteeseSettingsSend) String() string {
	return gg.JSON.Stringify(instance)
}

type MeeteeseSettingsRead struct {
	mailboxer_commons.MailboxerClientSettings
	Enabled                 bool   `json:"enabled"`
	RemoveUnhandledMessages bool   `json:"remove_unhandled_messages"`
	AttendeeEmail           string `json:"attendee_email"`
}

func (instance *MeeteeseSettingsRead) String() string {
	return gg.JSON.Stringify(instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//	s e t t i n g s
// ---------------------------------------------------------------------------------------------------------------------

func NewSettings(mode string) (*MeeteeseSettings, error) {
	instance := new(MeeteeseSettings)
	err := instance.init(mode)

	return instance, err
}

func (instance *MeeteeseSettings) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *MeeteeseSettings) init(mode string) error {
	if len(mode) == 0 {
		mode = "production"
	}
	dir := gg.Paths.WorkspacePath("./")
	filename := gg.Paths.Concat(dir, fmt.Sprintf("settings.%s.json", mode))

	// load settings
	return gg.JSON.ReadFromFile(filename, &instance)
}
