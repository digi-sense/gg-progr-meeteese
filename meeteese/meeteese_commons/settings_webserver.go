package meeteese_commons

type MeeteeseWebserverSettings struct {
	Enabled bool                   `json:"enabled"`
	Http    map[string]interface{} `json:"http"`
	Auth    *Authorization         `json:"auth"`
}

type Authorization struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}
