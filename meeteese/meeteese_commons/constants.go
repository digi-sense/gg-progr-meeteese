package meeteese_commons

import "errors"

const (
	AppName    = "Meeteese"
	AppVersion = "0.1.0"

	ModeProduction = "production"
	ModeDebug      = "debug"

	DateLayout = "2006-01-02T15:04:05.000Z"

	CommandPrefix = "cmd:"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e v e n t s
// ---------------------------------------------------------------------------------------------------------------------

const (
	// EventOnDoStop application is stopping
	EventOnDoStop = "on_do_stop"

	EventOnNewInvite           = "on_invite_new" // postman found invite into email and notify DB
	EventOnInviteNormalized    = "on_invite_normalized"
	EventOnInviteStatusChanged = "on_invite_status_changed" // database is confirming the event was saved or deleted

	EventOnMailCommand         = "on_mail_command"
	EventOnMailCommandResponse = "on_mail_command_response"
	EventOnMailSlotConfirmed   = "on_mail_slot_confirmed"
)

// ---------------------------------------------------------------------------------------------------------------------
//		w o r k s p a c e s
// ---------------------------------------------------------------------------------------------------------------------

const (
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"

	DirTemplates = "templates"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError   = errors.New("panic_system_error")
	AuthorizationError = errors.New("authorization_error")
	ExecutionError     = errors.New("execution_error")
)

var (
	HttpUnauthorizedError       = errors.New("unauthorized")          // 401
	HttpInvalidCredentialsError = errors.New("invalid_credentials")   // 401
	AccessTokenExpiredError     = errors.New("access_token_expired")  // 403
	RefreshTokenExpiredError    = errors.New("refresh_token_expired") // 401
	AccessTokenInvalidError     = errors.New("access_token_invalid")  // 401
	HttpUnsupportedApiError     = errors.New("unsupported_api")
)

// ---------------------------------------------------------------------------------------------------------------------
//	mail templates
// ---------------------------------------------------------------------------------------------------------------------

const (
	MailTemplateEventStatusChanged = "mailtpl_eventstatuschanged.html"
	MailTemplateCommandResponse    = "mailtpl_command_response.html"
)
