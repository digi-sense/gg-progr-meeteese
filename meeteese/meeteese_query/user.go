package meeteese_query

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"gorm.io/gorm"
)

// ---------------------------------------------------------------------------------------------------------------------
//	User
// ---------------------------------------------------------------------------------------------------------------------

func UserCreate(db *gorm.DB, parentId uint, email string, name string, surname, role, location string) (*model.User, error) {
	if len(location) == 0 {
		location = "Europe/Rome"
	}
	if len(role) == 0 {
		role = meeteese_commons.RoleApiEmail
	}
	user := new(model.User)
	user.Parent = parentId
	user.SetEmail(email)
	user.Location = location
	user.Surname = surname
	user.Role = role
	if len(name) > 0 {
		user.Name = name
	}
	tx := db.Create(&user)
	if nil != tx.Error {
		return nil, gg.Errors.Prefix(tx.Error, "Adding item to collection 'users'")
	}

	return user, nil
}

func UserGetOrCreate(db *gorm.DB, email string) (*model.User, error) {
	var user model.User
	tx := db.Where("uid = ?", model.NormalizeUid(email)).First(&user)
	if nil != tx.Error {
		if IsRecordNotFoundError(tx.Error) {
			user = model.User{}
			user.SetEmail(email)
			user.Location = "Europe/Rome"
			tx = db.Create(&user)
			if nil != tx.Error {
				return nil, gg.Errors.Prefix(tx.Error, "Adding item to collection 'users'")
			}
		} else {
			return nil, gg.Errors.Prefix(tx.Error, "Getting item from collection 'user'")
		}
	}
	return &user, nil
}

func UserGetByEmail(db *gorm.DB, email string) (user *model.User, err error) {
	tx := db.Where("uid = ?", model.NormalizeUid(email)).First(&user)
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		err = gg.Errors.Prefix(tx.Error, "Removing items from collection 'slots'")
	}
	return
}

func UserGetById(db *gorm.DB, id int) (user *model.User, err error) {
	tx := db.Where("id=?", id).First(&user)
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		err = gg.Errors.Prefix(tx.Error, "Removing items from collection 'slots'")
	}
	return
}
