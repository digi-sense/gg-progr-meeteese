package meeteese_query

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"gorm.io/gorm"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Slot
// ---------------------------------------------------------------------------------------------------------------------

// SlotGet get free slots for user
func SlotGet(db *gorm.DB, userId uint, from time.Time, rangeDays int) (response []model.Slot, err error) {
	start := from // gg.Dates.AddDays(date, rangeDays*-1)
	end := gg.Dates.AddDays(from, rangeDays)

	tx := db.Where("busy=false AND user_id = ? AND start_timestamp>=? AND end_timestamp<=?", userId, start.Unix(), end.Unix()).
		Find(&response)
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		err = gg.Errors.Prefix(tx.Error, "Removing items from collection 'slots'")
	}
	return
}

// SlotGetMultiple retrieve common slots between users
func SlotGetMultiple(db *gorm.DB, userIds []uint, date time.Time, rangeDays int) ([]model.Slot, error) {
	// get all
	all := make([][]model.Slot, 0)
	for _, userId := range userIds {
		slots, e := SlotGet(db, userId, date, rangeDays)
		if nil != e {
			return []model.Slot{}, e
		} else {
			all = append(all, slots)
		}
	}

	// get only commons
	response := make([]model.Slot, 0)
	for _, slots := range all {
		for _, item := range slots {
			if isCommon(item, all) {
				// response = append(response, item)
				response = gg.Arrays.AppendUniqueFunc(response, item, func(t interface{}, s interface{}) bool {
					tt := t.(model.Slot)
					ss := s.(model.Slot)
					return !(tt.StartAt.Equal(ss.StartAt) && tt.EndAt.Equal(ss.EndAt))
				}).([]model.Slot)
			}
		}
	}
	return response, nil
}

func SlotSetBusy(db *gorm.DB, id int, value bool) error {
	tx := db.Model(&model.Slot{}).Where("id = ?", id).Update("busy", value)
	if nil != tx.Error {
		if IsRecordNotFoundError(tx.Error) {
			return gg.Errors.Prefix(tx.Error, "Getting item from collection 'slots'")
		}
	}
	return nil
}

func SlotRemove(db *gorm.DB, eventId string, userId uint) error {
	tx := db.Where("event_id = ? AND user_id = ?", eventId, userId).Delete(&model.Slot{})
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		return gg.Errors.Prefix(tx.Error, "Removing items from collection 'slots'")
	}
	return nil
}

func SlotGetByKey(db *gorm.DB, id uint) (*model.Slot, error) {
	var slot *model.Slot
	tx := db.Where("id = ?", id).First(&slot)
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		return nil, gg.Errors.Prefix(tx.Error, "Getting item from collection 'slots' with a key")
	}
	return slot, nil
}

func SlotRemoveByKey(db *gorm.DB, id uint) error {
	tx := db.Where("id = ?", id).Delete(&model.Slot{})
	if nil != tx.Error && !IsRecordNotFoundError(tx.Error) {
		return gg.Errors.Prefix(tx.Error, "Removing items from collection 'slots'")
	}
	return nil
}

func SlotCreate(db *gorm.DB, eventId string, userId uint, title, description, link string, startAt, endAt time.Time) error {
	var slot = &model.Slot{
		Uid:         model.GetSlotUID(eventId, userId, startAt, endAt),
		EventId:     eventId,
		UserId:      userId,
		Busy:        false,
		Title:       title,
		Description: description,
		Link:        link,
	}

	slot.SetStartAt(startAt)
	slot.SetEndAt(endAt)

	tx := db.Create(slot)
	if nil != tx.Error {
		if tx.Error.Error() != "UNIQUE constraint failed: slots.uid" {
			return gg.Errors.Prefix(tx.Error, "Creating item into collection 'slots'")
		}
	}
	return nil
}

func SlotUpdate(db *gorm.DB, id uint, eventId string, title, description, link, inviteeEmail, inviteeName string, busy bool) error {
	slot := new(model.Slot)
	slot.ID = id
	slot.Busy = busy
	slot.InviteeName = inviteeName
	slot.InviteeMail = inviteeEmail
	if len(eventId) > 0 {
		slot.EventId = eventId // changing event ID protect the slot from be removed by calendar
	}
	if len(title) > 0 {
		slot.Title = title
	}
	if len(description) > 0 {
		slot.Description = description
	}
	if len(link) > 0 {
		slot.Link = link
	}
	tx := db.Model(slot).Where("id = ?", id).Updates(slot)
	if nil != tx.Error {
		return gg.Errors.Prefix(tx.Error, "Updating item from collection 'slots'")
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func isCommon(slot model.Slot, all [][]model.Slot) bool {
	count := 0
	for _, slots := range all {
		for _, item := range slots {
			equals := item.StartAt.Equal(slot.StartAt) && item.EndAt.Equal(slot.EndAt)
			if equals {
				count++
				break // next array
			}
		}
	}
	return count == len(all)
}
