package meeteese_query

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"fmt"
	"gorm.io/gorm"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Log
// ---------------------------------------------------------------------------------------------------------------------

func LogCreate(db *gorm.DB, userId uint, err error, message string, payload string) error {
	isError := nil != err
	if isError {
		if len(message) == 0 {
			message = err.Error()
		} else {
			message = fmt.Sprintf("%s - %s", message, err.Error())
		}
	}
	item := model.Log{
		UserId:  userId,
		Message: message,
		IsError: isError,
		Payload: payload,
	}
	tx := db.Create(&item)
	if nil != tx.Error {
		return gg.Errors.Prefix(tx.Error, "Adding item to collection 'logs'")
	}
	return nil
}
