package meeteese

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_scheduler"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_filters"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_initializer"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_postman"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_templates"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_xapi"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_xweb"
	"fmt"
	"path/filepath"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		A p p l i c a t i o n
// ---------------------------------------------------------------------------------------------------------------------

type Meeteese struct {
	mode    string
	root    string
	dirWork string

	settings    *meeteese_commons.MeeteeseSettings
	logger      *meeteese_commons.Logger
	stopChan    chan bool
	stopMonitor *stopMonitor
	events      *gg_events.Emitter

	scheduler     *gg_scheduler.Scheduler
	template      *meeteese_templates.MeeteeseTemplate
	postman       *meeteese_postman.MeeteesePostman
	database      *meeteese_database.MeeteeseDatabase
	commands      *meeteese_xapi.MeeteeseCommands
	datavalidator *meeteese_filters.MeeteeseDataValidator
	webcontroller *meeteese_xweb.MeeteeseWebController
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Meeteese) Start() (err error) {
	instance.stopChan = make(chan bool, 1)
	if nil != instance.stopMonitor {
		instance.stopMonitor.Start()
	}

	if nil != instance.scheduler {
		instance.scheduler.Start()
	}

	if nil != instance.webcontroller {
		instance.webcontroller.Start()
	}

	return
}

// Stop Try to close gracefully
func (instance *Meeteese) Stop() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}

	if nil != instance.scheduler {
		instance.scheduler.Stop()
	}

	if nil != instance.webcontroller {
		instance.webcontroller.Stop()
	}

	time.Sleep(3 * time.Second)
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}
	return
}

// Exit application
func (instance *Meeteese) Exit() (err error) {
	if nil != instance.stopMonitor {
		instance.stopMonitor.Stop()
	}
	if nil != instance.stopChan {
		instance.stopChan <- true
		instance.stopChan = nil
	}

	return
}

func (instance *Meeteese) Join() {
	if nil != instance.stopChan {
		<-instance.stopChan
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Meeteese) doStop(_ *gg_events.Event) {
	_ = instance.Exit()
}

func (instance *Meeteese) initSchedule() bool {
	if nil != instance && nil != instance.settings && nil != instance.settings.Schedule {
		schedule := instance.settings.Schedule
		instance.scheduler = gg_scheduler.NewScheduler()
		instance.scheduler.SetAsync(true) // sync messages
		instance.scheduler.AddSchedule(&gg_scheduler.Schedule{
			Uid:       schedule.Uid,
			StartAt:   schedule.StartAt,
			Timeline:  schedule.Timeline,
			Payload:   schedule.Payload,
			Arguments: schedule.Arguments,
		})
		instance.scheduler.OnSchedule(func(schedule *gg_scheduler.SchedulerTask) {
			instance.scheduler.Pause()
			defer instance.scheduler.Resume()
			instance.doSchedule()
		})
		return true
	}
	return false
}

func (instance *Meeteese) doSchedule() {
	if nil != instance && nil != instance.postman {
		// check if appointment email exists
		err := instance.postman.CheckEmail()
		if nil != err {
			instance.logger.Error(gg.Errors.Prefix(err, "Error checking email:"))
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func LaunchApplication(mode, cmdStop string, args ...interface{}) (instance *Meeteese, err error) {
	instance = new(Meeteese)
	instance.mode = mode

	// paths
	instance.dirWork = gg.Paths.GetWorkspace(meeteese_commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	// initialize environment
	err = meeteese_initializer.Initialize(mode)
	if nil != err {
		return
	}

	startedScheduler := false

	instance.settings, err = meeteese_commons.NewSettings(mode)
	if nil == err {
		instance.events = gg.Events.NewEmitter(meeteese_commons.AppName)
		instance.stopMonitor = newStopMonitor([]string{instance.root, instance.dirWork}, cmdStop, instance.events)
		instance.events.On(meeteese_commons.EventOnDoStop, instance.doStop)

		// logger as first parameter
		l := gg.Arrays.GetAt(args, 0, nil)
		instance.logger = meeteese_commons.NewLogger(mode, l)

		// initialize internal scheduler for action loop
		if startedScheduler = instance.initSchedule(); startedScheduler {
			// template engine
			instance.template = meeteese_templates.NewTemplate(instance.logger)

			// create the postman
			instance.postman, err = meeteese_postman.NewPostman(instance.settings.Postman, instance.logger, instance.events, instance.template)
			if nil != err {
				goto exit
			}
			// creates the database controller
			instance.database, err = meeteese_database.NewDatabaseController(instance.logger, instance.events)
			if nil != err {
				goto exit
			}
			// creates the command controller (used both for email and API)
			instance.commands = meeteese_xapi.NewMeeteeseCommands(instance.logger, instance.events,
				instance.database.DB())

			// API web controller
			instance.webcontroller = meeteese_xweb.NewMeeteeseWebController(mode, instance.logger, instance.commands)

			// FINALLY creates data validator filter
			instance.datavalidator = meeteese_filters.NewMeeteeseDataValidator(instance.logger, instance.events,
				instance.database.DB())
		}
	}

	// final log
exit:
	if nil == err {
		if startedScheduler {
			instance.logger.Info(fmt.Sprintf("STARTED '%s' v%s with schedule timeline: '%s'",
				meeteese_commons.AppName, meeteese_commons.AppVersion, instance.settings.Schedule.Timeline))
		} else {
			instance.logger.Warn(fmt.Sprintf("LAUNCHED '%s' v%s with no recurrent tasks!",
				meeteese_commons.AppName, meeteese_commons.AppVersion))
		}
		info := gg.Sys.GetInfo()
		instance.logger.Info(fmt.Sprintf("SYSTEM INFO: name=%v, kernel=%v, platform=%v, cpu=%v", info.Hostname, info.Kernel, info.Platform, info.CPUs))
		instance.logger.Info(fmt.Sprintf("MEMORY INFO: %v", info.MemoryUsage))
		process, e := gg.Sys.FindCurrentProcess()
		if nil == e {
			instance.logger.Info(fmt.Sprintf("CURRENT PROCESS PID: %v", process.Pid))
		}
	} else {
		instance.logger.Error(fmt.Sprintf("ERROR starting '%s' v%s: %s",
			meeteese_commons.AppName, meeteese_commons.AppVersion, err.Error()))
	}
	return
}
