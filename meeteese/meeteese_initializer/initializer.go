package meeteese_initializer

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	_ "embed"
	"fmt"
)

//go:embed default_settings.json
var DefaultSettings string

//go:embed default_mailtpl_eventstatuschanged.html
var DefaultMailTplEventStatusChanged string

//go:embed default_mailtpl_command_response.html
var DefaultMailTplCommandResponse string

//go:embed default_settings_webserver.json
var DefaultWebserverSettings string

//go:embed default_settings_websecure.json
var DefaultSecureSettings string

//go:embed default_html.cmd.get_slots.html
var DefaultHtmlTplGetSlotsList string

//go:embed default_html.cmd.error.html
var DefaultHtmlTplError string

func Initialize(mode string) (err error) {
	wpRoot := gg.Paths.WorkspacePath("./")
	tmpRoot := gg.Paths.Concat(wpRoot, "temp")

	gg.Paths.SetTempRoot(tmpRoot)
	gg.IO.RemoveAllSilent(tmpRoot)

	// settings
	filename := gg.Paths.Concat(wpRoot, fmt.Sprintf("settings.%s.json", mode))
	_ = gg.Paths.Mkdir(filename)
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultSettings, filename)
	}
	filename = gg.Paths.Concat(wpRoot, fmt.Sprintf("webserver.%s.json", mode))
	_ = gg.Paths.Mkdir(filename)
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultWebserverSettings, filename)
	}
	filename = gg.Paths.Concat(wpRoot, fmt.Sprintf("websecure.%s.json", mode))
	_ = gg.Paths.Mkdir(filename)
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(DefaultSecureSettings, filename)
	}

	// mail templates
	mailtplRoot := gg.Paths.Concat(wpRoot, meeteese_commons.DirTemplates, "mail")
	_ = gg.Paths.Mkdir(mailtplRoot + gg_utils.OS_PATH_SEPARATOR)
	// event status changed
	mailtplEventstatuschanged := gg.Paths.Concat(mailtplRoot, meeteese_commons.MailTemplateEventStatusChanged)
	write(mailtplEventstatuschanged, DefaultMailTplEventStatusChanged)
	// command
	filename = gg.Paths.Concat(mailtplRoot, meeteese_commons.MailTemplateCommandResponse)
	write(filename, DefaultMailTplCommandResponse)

	// html templates
	htmltplRoot := gg.Paths.Concat(wpRoot, meeteese_commons.DirTemplates, "html")
	_ = gg.Paths.Mkdir(htmltplRoot + gg_utils.OS_PATH_SEPARATOR)
	// get_slots.list
	filename = gg.Paths.Concat(htmltplRoot, fmt.Sprintf("cmd.%s.html", meeteese_commons.CmdGetSlotsFree))
	write(filename, DefaultHtmlTplGetSlotsList)
	// get_slots.list
	filename = gg.Paths.Concat(htmltplRoot, fmt.Sprintf("cmd.%s.html", "error"))
	write(filename, DefaultHtmlTplError)

	return
}

func write(filename, content string) {
	if b, _ := gg.Paths.Exists(filename); !b {
		_, _ = gg.IO.WriteTextToFile(content, filename)
	}
}
