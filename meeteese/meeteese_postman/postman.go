package meeteese_postman

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_filters"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_templates"
	"fmt"
	"strings"
	"sync"
	"time"
)

const removeHandledMessages = true

type MeeteesePostman struct {
	logger   *meeteese_commons.Logger
	events   *gg_events.Emitter
	settings *meeteese_commons.MeeteesePostmanSettings

	sync     sync.Mutex
	reader   *mailboxer_commons.MailBoxerClient
	template *meeteese_templates.MeeteeseTemplate

	removeUnhandledMessages bool
	attendeeEmail           string
}

func NewPostman(settings *meeteese_commons.MeeteesePostmanSettings, logger *meeteese_commons.Logger, events *gg_events.Emitter, template *meeteese_templates.MeeteeseTemplate) (*MeeteesePostman, error) {
	instance := new(MeeteesePostman)
	instance.settings = settings
	instance.logger = logger
	instance.events = events
	instance.template = template

	err := instance.init()
	if nil != err {
		return nil, err
	}

	return instance, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// CheckEmail check INBOX for meeting invitation
func (instance *MeeteesePostman) CheckEmail() error {
	if nil != instance {
		instance.sync.Lock()
		defer instance.sync.Unlock()

		toRemove := make([]uint32, 0) // emails to remove

		err := instance.openReader()
		if nil != err {
			return err
		}

		// get mailboxes
		mailboxes, err := instance.reader.ListMailboxes()
		if nil != err {
			return err
		}
		// loop all mailboxes
		for _, mailbox := range mailboxes {
			emails, e := instance.reader.ReadMailbox(mailbox.Name, true)
			if nil != e {
				continue
			}
			// check if email is a meeteese email
			for _, email := range emails {
				if isMeetingMessage(email) {
					email, e = instance.reader.ReadMessage(email.SeqNum)
					if nil == e && nil != email {
						if instance.trigger(mailbox.Name, email) {
							// remove
							toRemove = append(toRemove, email.SeqNum)
						} else {
							if instance.removeUnhandledMessages {
								toRemove = append(toRemove, email.SeqNum)
							}
						}
					}
				}
			}
		}

		instance.closeReader()

		// to remove
		err = instance.openReader()
		if nil != err {
			return err
		}
		mailboxes, err = instance.reader.ListMailboxes()
		if nil == err {
			for _, mailbox := range mailboxes {
				mailboxName := mailbox.Name
				emails, e := instance.reader.ReadMailbox(mailboxName, true)
				if nil != e {
					continue
				}
				for _, email := range emails {
					for _, seqNum := range toRemove {
						if seqNum == email.SeqNum {
							_ = instance.reader.MarkMessageAsDeleted(mailboxName, seqNum)
						}
					}
				}
			}

		}
		instance.closeReader()

	}
	return nil
}

// SendEventStatusChangedToUser Send confirmation message to meeteese user
func (instance *MeeteesePostman) SendEventStatusChangedToUser(to, uid, summary, description, status string, slots int, recurrent, cancelled bool, start, end time.Time) {
	// log.Println(action, to, slots, recurrent, start, end)
	if nil != instance && nil != instance.template {
		body, err := instance.template.GetMailEventStatusChanged(uid, summary, description, status, slots, recurrent, cancelled, start, end)
		if nil != err {
			// error getting template data
			instance.logger.Error(gg.Errors.Prefix(err, fmt.Sprintf("Error getting template '%s'", meeteese_commons.MailTemplateEventStatusChanged)))
			return
		}
		subject := fmt.Sprintf("EVENT %s: %s", status, summary)
		instance.sendAsync(to, subject, body, []string{})
	}
}

func (instance *MeeteesePostman) SendCommandResponse(response *meeteese_commons.MeeteeseApiResponse) {
	to := response.UserEmail
	subject := fmt.Sprintf("Received command: '%s'", response.Command.Statement)
	body, err := instance.template.GetMailCommandResponse(response)
	if nil != err {
		// error getting template data
		instance.logger.Error(gg.Errors.Prefix(err, fmt.Sprintf("Error getting template for command '%s'", response.Command.Statement)))
		return
	}

	// fmt.Println(to, subject, body)
	instance.sendAsync(to, subject, body, []string{})
}

// SendSlotConfirm
func (instance *MeeteesePostman) SendSlotConfirm(user *model.User, slot *model.Slot) {
	
	recipients := []string{user.Email, slot.InviteeMail}
	for _, to:=range recipients{
		// fmt.Println(to, subject, body)
		//instance.sendAsync(to, subject, body, []string{})
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteesePostman) init() (err error) {
	if nil != instance && nil != instance.settings {

		// mail
		if nil != instance.settings.Email {

			// reader for email
			if nil != instance.settings.Email.Read && instance.settings.Email.Read.Enabled {
				instance.attendeeEmail = instance.settings.Email.Read.AttendeeEmail
				if len(instance.attendeeEmail) == 0 {
					instance.attendeeEmail = instance.settings.Email.Read.Auth.User
				}
				instance.removeUnhandledMessages = instance.settings.Email.Read.RemoveUnhandledMessages
				instance.reader, err = mailboxer.NewClient(instance.settings.Email.Read.String())
				if nil != err {
					return
				}
				// test settings are working fine
				err = instance.openReader()
				instance.closeReader()
				if nil != err {
					return
				}

				instance.logger.Info(fmt.Sprintf("* Mail reader connected: host='%s', user='%s', attendee='%s'",
					instance.settings.Email.Read.Host, instance.settings.Email.Read.Auth.User, instance.settings.Email.Read.AttendeeEmail))
			} else {
				instance.logger.Info("* Mail reader NOT connected")
			}
		}

		// SMS
		if nil != instance.settings.SMS && instance.settings.SMS.Enabled {

		} else {
			instance.logger.Info("* SMS not enabled.")
		}

		// events
		if nil != instance.events {
			// subscribe
			instance.events.On(meeteese_commons.EventOnInviteStatusChanged, instance.onInviteStatusChanged)
			instance.events.On(meeteese_commons.EventOnMailCommandResponse, instance.onCommandResponse)
			instance.events.On(meeteese_commons.EventOnMailSlotConfirmed, instance.onSlotConfirmed)
		}
	} else {
		return gg.Errors.Prefix(meeteese_commons.PanicSystemError, "Missing Configuration for Postman!")
	}
	return
}

func (instance *MeeteesePostman) openReader() (err error) {
	if nil != instance && nil != instance.reader {
		err = instance.reader.Open()
	} else {
		return gg.Errors.Prefix(meeteese_commons.PanicSystemError, "Invalid internal state. Some services are not consistent")
	}
	return
}

func (instance *MeeteesePostman) closeReader() {
	if nil != instance && nil != instance.reader {
		_ = instance.reader.Close()
	}
}

func (instance *MeeteesePostman) trigger(mailBoxName string, mail *mailboxer_commons.MailboxerMessage) bool {
	subject := strings.ToLower(mail.Subject())
	if strings.HasPrefix(subject, meeteese_commons.CommandPrefix) {
		// found email command
		body := mail.Body.Text
		sender := mail.From()[0].Address
		instance.events.EmitAsync(meeteese_commons.EventOnMailCommand, subject, body, sender)
		return true
	} else {
		invite := mail.GetInvite()
		if nil != invite {
			if invite.IsValid() {
				if len(invite.EventOrganizer) > 0 && len(invite.EventAttendees) > 0 {
					// validate attendee, the target of this message
					if gg.Arrays.IndexOf(instance.attendeeEmail, invite.EventAttendees) > -1 {
						// this message is for meeteese
						return instance.handle(invite) // true - delete message
					}
				}
			} else {
				// log invalid invite
				instance.logger.Warn(fmt.Sprintf("INVALID INVITE IN MAILBOX '%s': %s", mailBoxName, invite.String()))
			}
		}
	}
	return false // message not handled. NOT REMOVED FROM MAILBOX
}

// handle an INVITE and update meeteese database
func (instance *MeeteesePostman) handle(invite *mailboxer_commons.MailboxerMessageInvite) bool {
	// emit event for database handler
	instance.events.EmitAsync(meeteese_commons.EventOnNewInvite, invite)

	return removeHandledMessages
}

// invite changed (new, remove, edit) and need a notification to user
func (instance *MeeteesePostman) onInviteStatusChanged(event *gg_events.Event) {
	if nil != instance {
		data := event.Argument(0)
		if invite, b := data.(*meeteese_filters.MeeteeseValidInvite); b {
			status := invite.Event.Status // CONFIRMED, CANCELLED
			to := invite.User.Email       //getEmailAddress(invite.EventOrganizer)
			uid := gg.Convert.ToString(invite.Id)
			summary := invite.Event.Summary
			description := invite.Event.Description
			slots := len(invite.Event.Slots)
			recurrent := invite.Event.IsRecurrent
			cancelled := invite.Event.IsCancelled
			start := invite.Event.StartAt
			end := invite.Event.EndAt
			instance.SendEventStatusChangedToUser(to, uid, summary, description, status, slots, recurrent, cancelled, start, end)
		}
	}
}

// received response from user mail-command
func (instance *MeeteesePostman) onCommandResponse(event *gg_events.Event) {
	if nil != instance {
		data := event.Argument(0)
		if response, b := data.(*meeteese_commons.MeeteeseApiResponse); b {
			instance.SendCommandResponse(response)
		}
	}
}

// client confirmed a slot. The slot has been set as busy, and now we must send an email with invite.ics
func (instance *MeeteesePostman) onSlotConfirmed(event *gg_events.Event) {
	if nil != instance {
		u := event.Argument(0)
		s := event.Argument(1)
		if user, ok := u.(*model.User); ok {
			if slot, ok := s.(*model.Slot); ok {
				instance.SendSlotConfirm(user, slot)
			}
		}
	}
}

func (instance *MeeteesePostman) sendAsync(to, subject, body string, attachments []string) {
	if nil != instance {
		go func() {
			err := instance.send(to, subject, body, attachments)
			if nil != err {
				if nil != instance.logger {
					instance.logger.Error(gg.Errors.Prefix(err, fmt.Sprintf("Error sending email from Postman "+
						"to '%s' with subject '%s': ", to, subject)))
				}
			}
		}()
	}
}

func (instance *MeeteesePostman) send(to, subject, body string, attachments []string) error {
	if nil != instance && nil != instance.settings && nil != instance.settings.Email && nil != instance.settings.Email.Send {
		settings := instance.settings.Email.Send
		if settings.Enabled {
			sender, err := gg.Email.NewSender(settings.String())
			if nil == err && nil != sender {
				return sender.Send(subject, body, []string{to}, settings.From, attachments)
			} else {
				return err
			}
		}
	}
	return nil
}
