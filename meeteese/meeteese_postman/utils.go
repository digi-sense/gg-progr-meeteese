package meeteese_postman

import (
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
	"net/mail"
)

func isMeetingMessage(message *mailboxer_commons.MailboxerMessage) bool {
	if nil != message && message.SeqNum > 0 && len(message.Subject()) > 0 {
		return true
	}
	return false
}

func getEmailAddress(email string) string {
	address, err := mail.ParseAddress(email)
	if nil != err {
		return email
	}
	return address.Address
}
