package meeteese_xweb

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_xapi"
	"github.com/gofiber/fiber/v2"
)

type MeeteeseWebController struct {
	root      string
	logger    *meeteese_commons.Logger
	commands  *meeteese_xapi.MeeteeseCommands
	webserver *Webserver
	websecure *Websecure
}

func NewMeeteeseWebController(mode string, logger *meeteese_commons.Logger, commands *meeteese_xapi.MeeteeseCommands) (instance *MeeteeseWebController) {
	root := gg.Paths.WorkspacePath("./webserver")
	_ = gg.Paths.Mkdir(root + gg_utils.OS_PATH_SEPARATOR)

	instance = new(MeeteeseWebController)
	instance.logger = logger
	instance.commands = commands
	instance.root = root

	instance.init(mode, root)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseWebController) Start() bool {
	if nil != instance {
		instance.webserver.Start()
		_ = instance.websecure.Start()
		return true
	}
	return false
}

func (instance *MeeteeseWebController) Stop() {
	if nil != instance {
		instance.webserver.Stop()
		instance.websecure.Stop()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseWebController) init(mode, root string) {
	instance.webserver = NewWebserver("webserver", mode, root)
	if nil != instance.webserver {
		instance.websecure = NewWebsecure(mode, instance.webserver.HttpAuth())

		// API DECLARATION
		instance.webserver.Handle("get", meeteese_commons.GET_SLOTS_FREE, instance.onGetSlotsFree)
		instance.webserver.Handle("get", meeteese_commons.GET_SLOTS_ONE, instance.onGetSlotsOne)
		instance.webserver.Handle("post", meeteese_commons.SLOTS_UPDATE, instance.onSlotUpdate)

		instance.webserver.Handle("post", meeteese_commons.USERS_CREATE, instance.onUsersCreate)
	}
}

// onGetSlots params: from, to, days, sender, users
func (instance *MeeteeseWebController) onGetSlotsFree(ctx *fiber.Ctx) error {
	if instance.websecure.AuthenticateRequest(ctx, true) {
		params := Params(ctx, true)
		userUid := gg.Reflect.GetString(params, "sender")
		response := instance.commands.Execute(meeteese_commons.RoleApiWeb,
			userUid, meeteese_commons.CmdGetSlotsFree, params)
		if response.Response.IsError() {
			return WriteResponse(ctx, nil, response.Response.GetError())
		} else {
			return WriteResponse(ctx, response.Response.Result, nil)
		}
	}
	return nil
}

func (instance *MeeteeseWebController) onGetSlotsOne(ctx *fiber.Ctx) error {
	if instance.websecure.AuthenticateRequest(ctx, true) {
		params := Params(ctx, true)
		userUid := gg.Reflect.GetString(params, "sender")
		response := instance.commands.Execute(meeteese_commons.RoleApiWeb,
			userUid, meeteese_commons.CmdGetSlotsOne, params)
		if response.Response.IsError() {
			return WriteResponse(ctx, nil, response.Response.GetError())
		} else {
			return WriteResponse(ctx, response.Response.Result, nil)
		}
	}
	return nil
}

func (instance *MeeteeseWebController) onUsersCreate(ctx *fiber.Ctx) error {
	if instance.websecure.AuthenticateRequest(ctx, true) {
		params := Params(ctx, true)
		userUid := gg.Reflect.GetString(params, "sender")
		response := instance.commands.Execute(meeteese_commons.RoleApiWeb,
			userUid, meeteese_commons.CmdUsersCreate, params)
		if response.Response.IsError() {
			return WriteResponse(ctx, nil, response.Response.GetError())
		} else {
			return WriteResponse(ctx, response.Response.Result, nil)
		}
	}
	return nil
}

// onSlotUpdate slot can be locked by a client or unlocked for renounce
func (instance *MeeteeseWebController) onSlotUpdate(ctx *fiber.Ctx) error {
	if instance.websecure.AuthenticateRequest(ctx, true) {
		params := Params(ctx, true)
		userUid := gg.Reflect.GetString(params, "sender")
		response := instance.commands.Execute(meeteese_commons.RoleApiWeb,
			userUid, meeteese_commons.CmdSlotsUpdate, params)
		if response.Response.IsError() {
			return WriteResponse(ctx, nil, response.Response.GetError())
		} else {
			return WriteResponse(ctx, response.Response.Result, nil)
		}
	}
	return nil
}
