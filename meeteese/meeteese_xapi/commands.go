package meeteese_xapi

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"fmt"
	"gorm.io/gorm"
	"strings"
)

type MeeteeseCommands struct {
	events   *gg_events.Emitter
	executor *MeeteeseExecutor
}

func NewMeeteeseCommands(logger *meeteese_commons.Logger, events *gg_events.Emitter, db *gorm.DB) (instance *MeeteeseCommands) {
	instance = new(MeeteeseCommands)
	instance.events = events
	instance.executor = &MeeteeseExecutor{
		logger: logger,
		db:     db,
	}

	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseCommands) Execute(referral, sender, command string, params map[string]interface{}) (response *meeteese_commons.MeeteeseApiResponse) {
	statement := new(meeteese_commons.MeeteeseApiStatement)
	statement.Statement = command
	statement.Params = params

	return instance.ExecuteCommand(referral, sender, statement)
}

func (instance *MeeteeseCommands) ExecuteCommand(referral, sender string, command *meeteese_commons.MeeteeseApiStatement) (response *meeteese_commons.MeeteeseApiResponse) {
	response = new(meeteese_commons.MeeteeseApiResponse)
	response.Referral = referral
	response.Sender = sender
	response.Command = command
	response.Response = new(meeteese_commons.MeeteeseApiStatementResponse)

	// should check user is registered and enabled to run command
	user, err := instance.getAuthUser(sender, referral)
	if nil != err {
		response.Response.Error = meeteese_commons.AuthorizationError.Error()
		response.Response.ErrorMessage = err.Error()
		return
	}
	response.UserId = user.ID
	response.UserEmail = user.Email

	switch command.Statement {
	//----------- SLOTS -----------
	case meeteese_commons.CmdGetSlotsFree:
		// get slots also retrieve multiple user's slots
		ids := instance.getUserIDs(user.ID, gg.Reflect.GetString(command.Params, "users"))
		if len(ids) > 1 {
			r, e := instance.executor.GetSlots(ids, command.Params)
			instance.initResult(response.Response, r, e)
		} else {
			r, e := instance.executor.GetSlots([]uint{user.ID}, command.Params)
			instance.initResult(response.Response, r, e)
		}
	case meeteese_commons.CmdGetSlotsOne:
		slotId := uint(gg.Reflect.GetInt(command.Params, "slot_id"))
		r, e := instance.executor.GetUserSlot(user.ID, slotId)
		instance.initResult(response.Response, r, e)
	case meeteese_commons.CmdSlotsUpdate:
		// required
		id := gg.Reflect.GetInt(command.Params, "slot_id")
		busy := gg.Reflect.GetBool(command.Params, "busy")
		inviteeMail := gg.Reflect.GetString(command.Params, "invitee_mail")
		inviteeName := gg.Reflect.GetString(command.Params, "invitee_name")
		// optional
		eventId := gg.Reflect.GetString(command.Params, "event_id")
		title := gg.Reflect.GetString(command.Params, "title")
		description := gg.Reflect.GetString(command.Params, "description")
		link := gg.Reflect.GetString(command.Params, "link")
		r, e := instance.executor.UpdateSlot(uint(id), eventId, title, description, link, inviteeMail, inviteeName, busy)
		instance.initResult(response.Response, r, e)
		// SEND EMAIL TO INVITEE WITH SLOT CONFIRMATION AND MEETING LINK IF ANY
		// email have invite.ics attached
		instance.events.Emit(meeteese_commons.EventOnMailSlotConfirmed, user, r)

	//----------- USERS -----------
	case meeteese_commons.CmdUsersCreate:
		if user.HasAuthForCommands(meeteese_commons.RoleApiCreateUser) {
			email := gg.Reflect.GetString(command.Params, "email")
			name := gg.Reflect.GetString(command.Params, "name")
			surname := gg.Reflect.GetString(command.Params, "surname")
			role := gg.Reflect.GetString(command.Params, "role")
			location := gg.Reflect.GetString(command.Params, "location")
			r, e := instance.executor.CreateUser(user.ID, email, name, surname, role, location)
			instance.initResult(response.Response, r, e)
		} else {
			err = gg.Errors.Prefix(meeteese_commons.AuthorizationError, "User is not authorized to create other Users: ")
			instance.initResult(response.Response, nil, err)
		}
	default:
		err = gg.Errors.Prefix(meeteese_commons.PanicSystemError, fmt.Sprintf("Command '%s' is not supported", command))
		instance.initResult(response.Response, nil, err)
	}

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseCommands) init() {
	instance.events.On(meeteese_commons.EventOnMailCommand, instance.onMailCommand)
}

func (instance *MeeteeseCommands) initResult(result *meeteese_commons.MeeteeseApiStatementResponse, response interface{}, err error) {
	if nil != err {
		result.Error = meeteese_commons.ExecutionError.Error()
		result.ErrorMessage = err.Error()
	} else {
		result.Result = response
	}
}

func (instance *MeeteeseCommands) getUserIDs(senderId uint, value string) []uint {
	response := make([]uint, 0)
	users, err := instance.getUsers(value)
	if nil == err {
		for _, user := range users {
			response = append(response, user.ID)
		}
	}
	response = gg.Arrays.AppendUnique(response, senderId).([]uint)
	return response
}

func (instance *MeeteeseCommands) getUsers(value string) ([]*model.User, error) {
	response := make([]*model.User, 0)
	tokens := gg.Strings.SplitTrim(value, ",;:", " ")
	for _, token := range tokens {
		user, err := instance.getUser(token)
		if nil != err {
			return nil, err
		}
		response = append(response, user)
	}
	return response, nil
}

func (instance *MeeteeseCommands) getUser(sender string) (*model.User, error) {
	var user *model.User
	var err error
	sender = strings.TrimSpace(sender)
	if gg.Regex.IsValidEmail(sender) {
		user, err = instance.executor.UserGetByEmail(sender)
	} else {
		user, err = instance.executor.UserGetById(sender)
	}
	if nil != err {
		err = gg.Errors.Prefix(meeteese_commons.AuthorizationError, "Error retrieving user from db: ")
		return nil, err
	}
	return user, nil
}

func (instance *MeeteeseCommands) getAuthUser(sender, referral string) (*model.User, error) {
	user, err := instance.getUser(sender)
	if nil != err {
		return nil, err
	}
	if nil == user || !user.HasAuthForCommands(referral) {
		err = gg.Errors.Prefix(meeteese_commons.AuthorizationError, "User is not authorized: ")
		return nil, err
	}
	return user, nil
}

func (instance *MeeteeseCommands) onMailCommand(event *gg_events.Event) {
	command := cleanCommand(event.ArgumentAsString(0))
	body := cleanBody(event.ArgumentAsString(1))
	senderEmail := event.ArgumentAsString(2)
	if len(command) > 0 && len(senderEmail) > 0 {
		// execute
		response := instance.Execute(meeteese_commons.RoleApiEmail, senderEmail, command, cleanParams(body))

		// send response to postman for user notification
		instance.events.EmitAsync(meeteese_commons.EventOnMailCommandResponse, response)
	}
}

func cleanCommand(text string) string {
	return strings.TrimSpace(strings.ReplaceAll(text, meeteese_commons.CommandPrefix, ""))
}

func cleanBody(text string) string {
	end := strings.Index(text, "--")
	if end > 0 {
		text = text[:end]
	}
	return strings.TrimSpace(text)
}

func cleanParams(text string) map[string]interface{} {
	response := make(map[string]interface{})
	tokens := gg.Strings.Split(text, "\n\r,")
	for _, token := range tokens {
		token = strings.ReplaceAll(token, "=", ":")
		pair := strings.Split(token, ":")
		if len(pair) == 2 {
			response[pair[0]] = pair[1]
		}
	}
	return response
}
