package meeteese_xapi

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_query"
	"fmt"
	"gorm.io/gorm"
	"math"
	"strings"
	"time"
)

type MeeteeseExecutor struct {
	logger *meeteese_commons.Logger
	db     *gorm.DB
}

// ---------------------------------------------------------------------------------------------------------------------
//	published
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseExecutor) GetSlots(userIds []uint, params map[string]interface{}) (interface{}, error) {
	from := time.Now()
	rangeDays := 5
	if v, ok := params["from"]; ok {
		s := strings.TrimSpace(gg.Convert.ToString(v))
		d, e := gg.Dates.ParseAny(fmt.Sprintf("%s", s))
		if nil == e {
			from = d
		}
	}
	if v, ok := params["to"]; ok {
		s := strings.TrimSpace(gg.Convert.ToString(v))
		d, e := gg.Dates.ParseAny(fmt.Sprintf("%s", s))
		if nil == e {
			h := d.Sub(from).Hours()
			if h > 0 {
				rangeDays = int(math.Max(h/24, 1))
			}
		}
	} else if v, ok := params["days"]; ok {
		s := strings.TrimSpace(gg.Convert.ToString(v))
		d := gg.Convert.ToInt(s)
		if d > -1 {
			rangeDays = d
		}
	}

	if len(userIds) > 0 {
		return meeteese_query.SlotGetMultiple(instance.db, userIds, from, rangeDays)
	} else {
		return meeteese_query.SlotGet(instance.db, userIds[0], from, rangeDays)
	}
}

func (instance *MeeteeseExecutor) GetUserSlot(userId, id uint) (interface{}, error) {
	slot, err := meeteese_query.SlotGetByKey(instance.db, id)
	if nil != err {
		return nil, err
	}
	// check slot has userId as owner
	if userId != slot.UserId {
		return nil, gg.Errors.Prefix(meeteese_commons.AuthorizationError, "User owner is different from request user: ")
	}
	return slot, nil
}

func (instance *MeeteeseExecutor) UpdateSlot(id uint, eventId, title, description, link, inviteeEmail, inviteeName string, busy bool) (interface{}, error) {
	err := meeteese_query.SlotUpdate(instance.db, id, eventId, title, description, link, inviteeEmail, inviteeName, busy)
	if nil != err {
		return nil, err
	}
	return meeteese_query.SlotGetByKey(instance.db, id)
}

func (instance *MeeteeseExecutor) CreateUser(id uint, email, name, surname, role, location string) (interface{}, error) {
	return meeteese_query.UserCreate(instance.db, id, email, name, surname, role, location)
}

// ---------------------------------------------------------------------------------------------------------------------
//	reserved
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseExecutor) UserGetByEmail(email string) (response *model.User, err error) {
	response, err = meeteese_query.UserGetByEmail(instance.db, email)
	return
}

func (instance *MeeteeseExecutor) UserGetById(id string) (response *model.User, err error) {
	response, err = meeteese_query.UserGetById(instance.db, gg.Convert.ToInt(id))
	return
}
