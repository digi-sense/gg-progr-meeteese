package meeteese_database

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_filters"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_query"
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"sync"
)

// MeeteeseDatabase Database controller
type MeeteeseDatabase struct {
	root   string
	logger *meeteese_commons.Logger
	events *gg_events.Emitter

	stopped bool
	dbSync  sync.Mutex
	dbMain  *gorm.DB
	dbLog   *gorm.DB
}

func NewDatabaseController(logger *meeteese_commons.Logger, events *gg_events.Emitter) (*MeeteeseDatabase, error) {
	instance := new(MeeteeseDatabase)
	instance.logger = logger
	instance.events = events

	instance.root = gg.Paths.WorkspacePath("./database/")
	err := instance.init()

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseDatabase) Close() {
	if nil != instance && nil != instance.dbMain {
		// close everything
		instance.stopped = true
		instance.dbMain = nil
	}
}

func (instance *MeeteeseDatabase) DB() *gorm.DB {
	if nil != instance {
		return instance.dbMain
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseDatabase) init() (err error) {
	instance.stopped = false

	// database initialization
	instance.dbMain, err = instance.initDatabase("meeteeseMain")
	if nil != err {
		return
	} else {
		err = instance.initSchemaMain(instance.dbMain)
		if nil != err {
			return
		}
	}

	instance.dbLog, err = instance.initDatabase("meeteeseLog")
	if nil != err {
		return
	} else {
		err = instance.initSchemaLog(instance.dbLog)
		if nil != err {
			return
		}
	}

	// event handler
	instance.events.On(meeteese_commons.EventOnDoStop, instance.onStop)
	instance.events.On(meeteese_commons.EventOnInviteNormalized, instance.onInvite)

	return
}

func (instance *MeeteeseDatabase) initDatabase(dbName string) (*gorm.DB, error) {
	filename := gg.Paths.Concat(instance.root, dbName+".db")
	_ = gg.Paths.Mkdir(filename)
	dsn := fmt.Sprintf("%s", filename)
	db, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, gg.Errors.Prefix(err, "Error Opening Database")
	}
	return db, nil
}

func (instance *MeeteeseDatabase) initSchemaMain(db *gorm.DB) error {
	// init schemas
	err := db.AutoMigrate(&model.User{})
	if err != nil {
		return gg.Errors.Prefix(err, "Error Migrating Schema 'User'")
	}
	err = db.AutoMigrate(&model.Slot{})
	if err != nil {
		return gg.Errors.Prefix(err, "Error Migrating Schema 'Slot'")
	}

	return nil
}

func (instance *MeeteeseDatabase) initSchemaLog(db *gorm.DB) error {
	// init schemas
	err := db.AutoMigrate(&model.Log{})
	if err != nil {
		return gg.Errors.Prefix(err, "Error Migrating Schema 'Log'")
	}
	return nil
}

func (instance *MeeteeseDatabase) onStop(_ *gg_events.Event) {
	if nil != instance {
		instance.Close()
	}
}

func (instance *MeeteeseDatabase) onInvite(event *gg_events.Event) {
	if nil != instance && !instance.stopped {
		data := event.Argument(0)
		if invite, b := data.(*meeteese_filters.MeeteeseValidInvite); b {
			instance.handle(invite)
		}
	}
}

func (instance *MeeteeseDatabase) handle(invite *meeteese_filters.MeeteeseValidInvite) {
	instance.dbSync.Lock()
	defer instance.dbSync.Unlock()

	// retrieve an user
	user := invite.User

	// get event id
	if invite.IsInternal {
		// email sent as response to internal existing slot
		// id is like: "slot:1234"
		// The client want to cancel the event
		fmt.Println(invite.Id, "INTERNAL")
		if invite.Event.IsCancelled {
			// set the slot "busy" field to false
			_ = meeteese_query.SlotSetBusy(instance.dbMain, gg.Convert.ToInt(invite.Id), false)
		} else {
			// NOT SUPPORTED YET.
			// TODO: implement slot changes if not in conflict with other slots
		}
	} else {
		// calendar event used to set meeteese internal slots
		// fmt.Println(id, "STANDARD", user)
		if invite.Event.IsCancelled {
			// REMOVE ALL SLOTS
			err := meeteese_query.SlotRemove(instance.dbMain, gg.Convert.ToString(invite.Id), user.ID)
			if nil != err {
				instance.logger.Error(gg.Errors.Prefix(err, "Error handling invite: "))
				// log into db
				_ = meeteese_query.LogCreate(instance.dbLog, user.ID, err, "", fmt.Sprintf("%s: %s",
					invite.Event.Status, invite.Event.Summary))
				return
			} else {
				// notify CANCELLED status
				instance.events.EmitAsync(meeteese_commons.EventOnInviteStatusChanged, invite)
				// log into db
				_ = meeteese_query.LogCreate(instance.dbLog, user.ID, nil, fmt.Sprintf("%s: %s",
					invite.Event.Status, invite.Event.Summary), invite.String())
			}
		} else {
			// CREATES SLOTS
			link := invite.Event.Link
			slots := invite.Event.Slots
			creationErrors := make([]error, 0)
			for _, slot := range slots {
				err := meeteese_query.SlotCreate(instance.dbMain, gg.Convert.ToString(invite.Id), user.ID,
					invite.Event.Summary, invite.Event.Description, link, slot.StartAt, slot.EndAt)
				if nil != err {
					creationErrors = append(creationErrors, err)
					instance.logger.Error(gg.Errors.Prefix(err, "Error handling invite during Slot creation: "))
					return
				}
			}
			if len(creationErrors) == 0 {
				// notify CONFIRMED status
				instance.events.EmitAsync(meeteese_commons.EventOnInviteStatusChanged, invite)
				// log into db
				_ = meeteese_query.LogCreate(instance.dbLog, user.ID, nil, fmt.Sprintf("%s: %s",
					invite.Event.Status, invite.Event.Summary), invite.String())
			} else {
				_ = meeteese_query.LogCreate(instance.dbLog, user.ID, creationErrors[0], fmt.Sprintf("MANY ERRORS (%v)", len(creationErrors)), fmt.Sprintf("%s: %s",
					invite.Event.Status, invite.Event.Summary))
			}
		}
	}
}
