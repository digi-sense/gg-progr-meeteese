package model

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
	"fmt"
	"strings"
	"time"
)

func NormalizeUid(value string) string {
	return gg.Coding.MD5(strings.ToLower(value))
}

// ---------------------------------------------------------------------------------------------------------------------
//	invite
// ---------------------------------------------------------------------------------------------------------------------

func GetIdFromInvite(invite *mailboxer_commons.MailboxerMessageInvite) (IsInternal bool, response interface{}) {
	eventUID := invite.EventUID
	if IsInternal, response = ParseSlotId(eventUID); IsInternal {
		return
	} else {
		return false, GetInviteId(invite.EventOrganizer, invite.EventUID)
	}
}

func GetInviteId(user, uid string) string {
	return NormalizeUid(fmt.Sprintf("%s-%s", user, uid))
}

// ---------------------------------------------------------------------------------------------------------------------
//	slot
// ---------------------------------------------------------------------------------------------------------------------

func GetSlotUID(eventID string, userId uint, start, end time.Time) string {
	uid := fmt.Sprintf("%s-%v-%v-%v", eventID, userId, start.Unix(), end.Unix())
	return NormalizeUid(uid)
}

// GetSlotId used when sent email for slot busy confirmation
// EVENT-ID must be identified for further cancellation
func GetSlotId(id int) string {
	return fmt.Sprintf("slot:%v", id)
}

func IsSlotId(id string) bool {
	return strings.Index(id, "slot:") == 0
}

func ParseSlotId(id string) (bool, interface{}) {
	if IsSlotId(id) {
		tokens := strings.Split(id, ":")
		return true, gg.Convert.ToInt(tokens[1])
	}
	return false, id
}
