package model

import (
	"fmt"
	"time"
)

// Slot when a slot is made busy from a user, the Id declared in email is "slot:1234",
// where "slot:" is a prefix that identify and distinguish a standard event from internal event
type Slot struct {
	ID             uint `gorm:"primarykey"`
	CreatedAt      time.Time
	UpdatedAt      time.Time
	Uid            string    `gorm:"index:idx_uid,unique"`
	UserId         uint      `gorm:"index:idx_user_event"`
	EventId        string    `gorm:"index:idx_user_event"`
	StartAt        time.Time `gorm:"index:idx_start_at"`
	EndAt          time.Time `gorm:"index:idx_end_at"`
	StartTimestamp int64     `gorm:"index:idx_start_timestamp"` // get slots
	EndTimestamp   int64     `gorm:"index:idx_end_timestamp"`   // get slots
	StartYear      int       `gorm:"index:idx_start"`
	StartMonth     int       `gorm:"index:idx_start"`
	StartDay       int       `gorm:"index:idx_start"`
	StartHour      int       `gorm:"index:idx_start"`
	StartMinute    int       `gorm:"index:idx_start"`
	StartWeekday   string
	EndYear        int `gorm:"index:idx_end"`
	EndMonth       int `gorm:"index:idx_end"`
	EndDay         int `gorm:"index:idx_end"`
	EndHour        int `gorm:"index:idx_end"`
	EndMinute      int `gorm:"index:idx_end"`
	EndWeekday     string
	Busy           bool `gorm:"index:idx_busy"`
	Duration       time.Duration
	Title          string
	Description    string
	Link           string
	InviteeName    string
	InviteeMail    string
}

func (instance *Slot) SetStartAt(t time.Time) {
	instance.StartAt = t
	instance.Refresh()
}

func (instance *Slot) SetEndAt(t time.Time) {
	instance.EndAt = t
	instance.Refresh()
}

func (instance *Slot) Refresh() {
	instance.StartTimestamp = instance.StartAt.Unix()
	instance.EndTimestamp = instance.EndAt.Unix()

	instance.StartYear = instance.StartAt.Year()
	instance.StartMonth = int(instance.StartAt.Month())
	instance.StartDay = instance.StartAt.Day()
	instance.StartHour = instance.StartAt.Hour()
	instance.StartMinute = instance.StartAt.Minute()
	instance.StartWeekday = fmt.Sprintf("%v", instance.StartAt.Weekday())

	instance.EndYear = instance.EndAt.Year()
	instance.EndMonth = int(instance.EndAt.Month())
	instance.EndDay = instance.EndAt.Day()
	instance.EndHour = instance.EndAt.Hour()
	instance.EndMinute = instance.EndAt.Minute()
	instance.EndWeekday = fmt.Sprintf("%v", instance.EndAt.Weekday())

	instance.Duration = instance.EndAt.Sub(instance.StartAt)
}
