package model

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"gorm.io/gorm"
	"net/mail"
	"strings"
)

type User struct {
	gorm.Model
	Parent   uint
	Uid      string `gorm:"index:uid,unique"`
	Email    string `gorm:"index:email"`
	Name     string
	Surname  string
	Location string
	Role     string
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *User) IsSuper() bool {
	return instance.Email == "angelo.geminiani@gmail.com" || instance.hasRole("super")
}

func (instance *User) HasAuthForCommands(referrals string) bool {
	return instance.IsSuper() ||
		instance.hasRole(referrals) ||
		instance.hasRole(meeteese_commons.RoleApiAll)
}

// SetEmail parse address "Alice <alice@example.com>"
func (instance *User) SetEmail(value string) {
	address, err := mail.ParseAddress(value)
	if nil != err {
		instance.Email = value
	} else {
		instance.Uid = NormalizeUid(address.Address)
		instance.Email = address.Address
		name := strings.Split(address.Name, " ")
		instance.Name = name[0]
		if len(name) > 1 {
			instance.Surname = name[1]
		}
	}
}

func (instance *User) hasRole(role string) bool {
	roles := strings.Split(instance.Role, "|")
	return gg.Arrays.IndexOf(role, roles) > -1
}
