package meeteese_filters

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"time"
)

type MeeteeseValidInvite struct {
	Id         interface{}         `json:"id"`
	IsInternal bool                `json:"is_internal"`
	User       *model.User         `json:"user"`
	Event      *MeeteeseValidEvent `json:"event"`
}

func (instance *MeeteeseValidInvite) String() string {
	return gg.JSON.Stringify(instance)
}

type MeeteeseValidEvent struct {
	Name        string                    `json:"name"`
	Summary     string                    `json:"summary"`
	Description string                    `json:"description"`
	Product     string                    `json:"product"`
	Vendor      string                    `json:"vendor"`
	Status      string                    `json:"status"`
	Link        string                    `json:"link"`
	IsCancelled bool                      `json:"is_cancelled"`
	StartAt     time.Time                 `json:"start_at"`
	EndAt       time.Time                 `json:"end_at"`
	Duration    time.Duration             `json:"duration"`
	IsRecurrent bool                      `json:"is_recurrent"`
	Slots       []*MeeteeseValidEventSlot `json:"slots"`
}

type MeeteeseValidEventSlot struct {
	StartAt  time.Time     `json:"start_at"`
	EndAt    time.Time     `json:"end_at"`
	Duration time.Duration `json:"duration"`
}
