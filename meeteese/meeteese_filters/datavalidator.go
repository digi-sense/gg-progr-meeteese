package meeteese_filters

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_database/model"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_query"
	"gorm.io/gorm"
	"time"
)

type MeeteeseDataValidator struct {
	logger *meeteese_commons.Logger
	db     *gorm.DB
	events *gg_events.Emitter
}

func NewMeeteeseDataValidator(logger *meeteese_commons.Logger, events *gg_events.Emitter, db *gorm.DB) (instance *MeeteeseDataValidator) {
	instance = new(MeeteeseDataValidator)
	instance.logger = logger
	instance.events = events
	instance.db = db

	instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseDataValidator) init() {
	instance.events.On(meeteese_commons.EventOnNewInvite, instance.onInvite)
}

func (instance *MeeteeseDataValidator) onInvite(event *gg_events.Event) {
	if nil != instance {
		data := event.Argument(0)
		if invite, b := data.(*mailboxer_commons.MailboxerMessageInvite); b {
			// normalize before send notification
			instance.notify(instance.normalize(invite))
		} else if valid, b := data.(*MeeteeseValidInvite); b {
			// ready to notification
			instance.notify(valid)
		}
	}
}

func (instance *MeeteeseDataValidator) normalize(invite *mailboxer_commons.MailboxerMessageInvite) *MeeteeseValidInvite {
	// retrieve an user
	user, err := meeteese_query.UserGetOrCreate(instance.db, invite.EventOrganizer)
	if nil != err {
		// database error
		instance.logger.Error(gg.Errors.Prefix(err, "Error handling invite: "))
		return nil
	}
	if nil == user {
		return nil
	}

	// update invite date time using user location for daylight adjustment
	location := user.Location
	if len(location) == 0 {
		location = "Europe/Rome"
	}

	event := new(MeeteeseValidEvent)
	event.Name = invite.Name
	event.Summary = invite.EventSummary
	event.Description = invite.EventDescription
	event.Product = invite.Product
	event.Vendor = invite.Vendor
	event.Status = invite.EventStatus
	event.IsCancelled = invite.EventIsCancelled
	event.IsRecurrent = invite.EventIsRecurrent
	event.Duration = invite.EventDuration
	event.StartAt = instance.daylight(invite.EventStartAt, location, event.Product, event.Vendor, event.IsRecurrent)
	event.EndAt = instance.daylight(invite.EventEndAt, location, event.Product, event.Vendor, event.IsRecurrent)
	event.Link = invite.EventUrl

	for _, slot := range invite.EventSlots {
		start := slot.StartAt
		end := slot.EndAt
		eSlot := &MeeteeseValidEventSlot{
			StartAt:  instance.daylight(start, location, event.Product, event.Vendor, event.IsRecurrent),
			EndAt:    instance.daylight(end, location, event.Product, event.Vendor, event.IsRecurrent),
			Duration: slot.Duration,
		}
		event.Slots = append(event.Slots, eSlot)
	}

	// send to final handler (database)
	isInternal, id := model.GetIdFromInvite(invite)
	return &MeeteeseValidInvite{
		Id:         id,
		IsInternal: isInternal,
		User:       user,
		Event:      event,
	}
}

func (instance *MeeteeseDataValidator) daylight(date time.Time, location, product, vendor string, isRecurrent bool) time.Time {
	if !isRecurrent {
		// google and microsoft in non-recurrent events do not apply daylight time
		if vendor == "google" || vendor == "microsoft" {
			return gg.Dates.AdjustDaylight(date, location)
		}
	}
	return date
}
func (instance *MeeteeseDataValidator) notify(invite *MeeteeseValidInvite) {
	if nil != instance && nil != invite {
		instance.events.EmitAsync(meeteese_commons.EventOnInviteNormalized, invite)
	}
}
