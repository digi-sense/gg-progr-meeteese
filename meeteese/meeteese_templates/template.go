package meeteese_templates

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-meeteese/meeteese/meeteese_commons"
	"fmt"
	"github.com/cbroglie/mustache"
	"time"
)

const projectSourceUrl = "https://bitbucket.org/digi-sense/gg-progr-meeteese/src/master/readme.md"
const projectSiteUrl = "https://bitbucket.org/digi-sense/gg-progr-meeteese/src/master/readme.md"
const projectLogo = "https://bitbucket.org/digi-sense/gg-progr-meeteese/raw/c074d78a5cf15cab45bbf95a6a433ce62e0d749d/icon_128.png"

type MeeteeseTemplate struct {
	logger *meeteese_commons.Logger
	root   string
}

func NewTemplate(logger *meeteese_commons.Logger) *MeeteeseTemplate {
	instance := new(MeeteeseTemplate)
	instance.logger = logger
	instance.root = gg.Paths.WorkspacePath(meeteese_commons.DirTemplates)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	e m a i l
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseTemplate) GetMailEventStatusChanged(uid, summary, description, status string, slots int, recurrent, cancelled bool, start, end time.Time) (text string, err error) {
	text, err = instance.readMail(meeteese_commons.MailTemplateEventStatusChanged)
	if nil != err {
		return "", err
	}
	data := instance.data()
	data["uid"] = uid
	data["summary"] = summary
	data["description"] = description
	data["status"] = status
	data["slots"] = slots
	data["recurrent"] = recurrent
	data["cancelled"] = cancelled
	data["start"] = gg.Formatter.FormatDate(start, "yyyy-MM-dd at HH:mm")
	data["end"] = gg.Formatter.FormatDate(end, "yyyy-MM-dd at HH:mm")

	text, _ = mustache.Render(text, data) //gg.Formatter.Merge(text, data)

	return
}

func (instance *MeeteeseTemplate) GetMailCommandResponse(response *meeteese_commons.MeeteeseApiResponse) (text string, err error) {
	text, err = instance.readMail(meeteese_commons.MailTemplateCommandResponse)
	if nil != err {
		return "", err
	}

	data := instance.data()
	data["content"] = ""

	// html widget
	content, e := instance.GetHtmlCommandResponse(response)
	if nil != e {
		data["content"] = e.Error()
	} else {
		data["content"] = content
	}

	text, _ = mustache.Render(text, data)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	h t m l
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseTemplate) GetHtmlCommandResponse(response *meeteese_commons.MeeteeseApiResponse) (text string, err error) {
	if nil != response {
		var name string
		model := map[string]interface{}{
			"user_email": response.UserEmail,
			"command":    response.Command.Statement,
			"params":     gg.Convert.ToString(response.Command.Params),
		}
		if response.Response.IsError() {
			// error
			name = "cmd.error.html"
			model["code"] = response.Response.Error
			model["message"] = response.Response.ErrorMessage
			model["result"] = nil
		} else {
			name = fmt.Sprintf("cmd.%s.html", response.Command.Statement)
			model["code"] = ""
			model["message"] = ""
			model["result"] = response.Response.Result
		}
		text, err = instance.readHTML(name)
		if nil == err {
			text, _ = mustache.Render(text, model)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MeeteeseTemplate) rootMail() string {
	return gg.Paths.Concat(instance.root, "mail")
}

func (instance *MeeteeseTemplate) rootHTML() string {
	return gg.Paths.Concat(instance.root, "html")
}

func (instance *MeeteeseTemplate) read(filename string) (response string, err error) {
	response, err = gg.IO.ReadTextFromFile(filename)
	return
}

func (instance *MeeteeseTemplate) readMail(filename string) (response string, err error) {
	response, err = gg.IO.ReadTextFromFile(gg.Paths.Concat(instance.rootMail(), filename))
	return
}

func (instance *MeeteeseTemplate) readHTML(filename string) (response string, err error) {
	response, err = gg.IO.ReadTextFromFile(gg.Paths.Concat(instance.rootHTML(), filename))
	return
}

func (instance *MeeteeseTemplate) data() map[string]interface{} {
	m := make(map[string]interface{})
	m["project_site_url"] = projectSiteUrl
	m["project_source_url"] = projectSourceUrl
	m["project_logo"] = projectLogo
	return m
}
